CREATE TABLE `user` (
  `userId` varchar(10) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `accountStatus` varchar(255) NOT NULL,
  `accountType` varchar(255) NOT NULL,
  `dateOfBirth` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `lastLoginTime` datetime DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phoneNumber` bigint(20) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `securityQuestion` varchar(255) NOT NULL,
  `siteKey` longblob,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `accountType` (`accountType`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `phoneNumber` (`phoneNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `transaction` (
  `transactionId` bigint(20) NOT NULL,
  `amount` double DEFAULT NULL,
  `receiverId` varchar(255) NOT NULL,
  `senderId` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `timeStamp` datetime DEFAULT NULL,
  PRIMARY KEY (`transactionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `systemadmin` (
  `userId` varchar(255) NOT NULL,
  `authorization` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `roles` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `role` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `merchant` (
  `userId` varchar(255) NOT NULL,
  `accountId` varchar(255) NOT NULL,
  `authorization` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `accountId` (`accountId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `employee_roles` (
  `employee_Id` bigint(20) NOT NULL,
  `roles_Id` bigint(20) NOT NULL,
  PRIMARY KEY (`employee_Id`,`roles_Id`),
  KEY `fk_r_id` (`roles_Id`),
  KEY `fk_e_id` (`employee_Id`),
  CONSTRAINT `fk_e_id` FOREIGN KEY (`employee_Id`) REFERENCES `employee` (`Id`),
  CONSTRAINT `fk_r_id` FOREIGN KEY (`roles_Id`) REFERENCES `roles` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `employee` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `userId` varchar(255) NOT NULL,
  `authorization` varchar(255) NOT NULL,
  `departmentId` varchar(255) NOT NULL,
  `salary` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `department` (
  `departmentId` varchar(255) NOT NULL,
  `PIIAuthorizaton` varchar(255) NOT NULL,
  `authorization` varchar(255) NOT NULL,
  `departmentName` varchar(255) NOT NULL,
  `managerID` varchar(255) NOT NULL,
  PRIMARY KEY (`departmentId`),
  UNIQUE KEY `departmentName` (`departmentName`),
  UNIQUE KEY `managerID` (`managerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customers` (
  `UserID` varchar(255) NOT NULL,
  `AccountID` varchar(255) NOT NULL,
  `Authorization` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `AccountID` (`AccountID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `account` (
  `accountID` varchar(10) NOT NULL,
  `accountType` varchar(255) NOT NULL,
  `checkingBalance` double DEFAULT NULL,
  `savingBalance` double DEFAULT NULL,
  `userType` varchar(255) NOT NULL,
  PRIMARY KEY (`accountID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

