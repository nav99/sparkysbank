<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="RegularEmployee.jsp"></jsp:include>
<br/><br/>
<h4>Authorize/unauthorize System Administrator to have access rights.</h4>
<form:form method="POST">
  	 <td><input type="submit" onClick="this.form.action='${pageContext.servletContext.contextPath}/Employee_AuthorizeSystemAdmin'" value="Authorize System Admin"></td>
  	 <td><input type="submit" onClick="this.form.action='${pageContext.servletContext.contextPath}/Employee_UnauthorizeSystemAdmin'" value="Unauthorize System Admin"></td>
</form:form> 
</body>
</html>