<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>View Transactions/ Processing Transactions</title>
</head>
<body>
 <jsp:include page="RegularEmployee.jsp"></jsp:include>
 <br/>
 <br/>
<h4 align="center">Modify Delete or Approve Transactions</h4>
<table border="1" cellpadding="5" cellspacing="5">
 <tr bgcolor="#CCCCCC">
  <td width="150">Transaction ID</td>
  <td width="150">Amount</td>
  <td width="150">Sender ID</td>
  <td width="150">Receiver ID</td>
  <td width="70">Modify</td>
  <td width="70">Delete</td>
  <td width="70">Approve</td>
 </tr>
 
<c:forEach items="${transactions}" var="transaction">
  <tr>
   <td><c:out value="${transaction.transactionId}" /></td>
   <td><c:out value="${transaction.amount}" /></td>
   <td><c:out value="${transaction.senderId}" /></td>
   <td><c:out value="${transaction.receiverId}" /></td>
   <form:form method="POST"  modelAttribute="transactionDummy" commandName="transactionDummy" action="${pageContext.servletContext.contextPath}/EmployeeTransactions_ModifyTransaction">
 	  <td><input type="submit" value="Modify"/></td>
  	 <form:hidden path="transactionId" value="${transaction.transactionId}" />
  	 <td><input type="submit" onClick="this.form.action='${pageContext.servletContext.contextPath}/EmployeeTransactions_DeleteTransaction'" value="Delete"></td>
  	 <td><input type="submit" onClick="this.form.action='${pageContext.servletContext.contextPath}/EmployeeTransactions_ApproveTransaction'" value="Approve"></td>
  	 </form:form> 
  </tr>
</c:forEach>
</table>
</body>
</html>