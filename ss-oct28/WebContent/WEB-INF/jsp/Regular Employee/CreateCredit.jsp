<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create Transaction</title>
</head>
<body>
<jsp:include page="RegularEmployee.jsp"></jsp:include>
<br/><br/>
<h4>Enter below details to credit to the account.</h4>
<form:form modelAttribute="transactionModel" method="POST" action="${pageContext.servletContext.contextPath}/EmployeeTransactions_CreateCredit">
<p> "Your Account ID :" <c:out value="${accountId}"/></p>
	<table border="1" cellpadding="5" cellspacing="5">
	  <tr>
	   <td bgcolor="#CCCCCC"><form:label path="receiverId">Receiver Account ID:</form:label></td>
	   <td><form:input path="receiverId"/></td>
	  </tr>
	
	  <tr>
	   <td bgcolor="#CCCCCC"><form:label path="amount">Amount:</form:label></td>
	   <td><form:input path="amount"/></td>
	  </tr>
	</table>
	<br>
	<input type="submit" value="Credit">
</form:form>

</body>
</html>