<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create Transaction</title>
</head>
<body>
<jsp:include page="RegularEmployee.jsp"></jsp:include>
<br/>
<br/>
<h4>Choose one of the below services to perform operations for an authorized user.</h4>
<form:form method="GET">
	<td><input type="submit" onClick="this.form.action='${pageContext.servletContext.contextPath}/EmployeeTransactions_InitiateCreateCredit'" value="Credit"></td>
	<td><input type="submit" onClick="this.form.action='${pageContext.servletContext.contextPath}/EmployeeTransactions_InitiateCreateDebit'" value="Debit"></td>
	<td><input type="submit" onClick="this.form.action='${pageContext.servletContext.contextPath}/EmployeeTransactions_InitiateCreateTransfer'" value="Transfer"></td>
</form:form> 

</body>
</html>