<%@page session="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
      <meta http-equiv="content-type" content="text/html; charset=UTF-8">
      <title>Login failed</title>
  </head>
<body>
<div id="content">
<h2>Login failed</h2>
<p>
Login failed. Please check your username and password.
<p><a href="${pageContext.servletContext.contextPath}/login">Sign in again</a></p>
</div>
</body>
</html>
