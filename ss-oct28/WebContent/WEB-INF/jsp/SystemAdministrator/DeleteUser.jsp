<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Account Info</title>
<script>
function validateForm()
{
var x=document.forms["addUser"]["UserID"].value;
if (x==null || x=="")
  { 
  alert("Please enter UserID");
  return false;
  }
}
</script>
</head>
<body>
<jsp:include page="ManageUserAccounts.jsp" />
<br/><br>
<form name="addUser" method="POST" onsubmit="return validateForm()" action="SystemDeleteUserSubmit.html">
<br>
<b>
<c:out value="${message}"/>
</b>
<br/>
<br/>
<table border="1">
<tr>
<td>User ID:</td>
<td><input type="text" name="UserID" id="UserID"><br></td>
<td><button type="submit">Delete User</button></td>
</tr>
</table>
</form>

</body>
</html>