<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 
<head>
    <script src="JavaScript/jquery-1.6.1.min.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add User</title>
<script>
function validateForm(theForm)
{
var userid=theForm.UserID.value;
var name=theForm.Name.value;
/*var pwd=theForm.Password.value;
var birthdate=theForm.dateOfBirth.value;
var email=theForm.email.value;
var address=theForm.address.value;
var phone=theForm.phoneNumber.value;
var question=theForm.securityQuestion.value;
var ans=theForm.Answer.value;
var bal=theForm.SavingAccount.value; */

if (userid==null || userid=="")
  { 
  alert("Please enter UserID");
  return false;
  }
if (name==null || name=="")
{ 
alert("Please enter Name");
return false;
}
}
</script>

</head>
<body>

<jsp:include page="ManageUserAccounts.jsp" />
<br><br>
<form name="addUser" method="POST" onsubmit="return validateForm(this)" action="SystemAddUserSubmit.html">
<br>
<b>
<c:out value="${message}"/>
</b>
<br/><br>
<table border="1">
<tr>
<td>User ID:</td>
<td><input type="text" name="UserID" ID="UserID"><br></td>
</tr>
<tr>
<td>Name:</td>
<td><input type="text" name="Name" ID="Name"><br></td>
</tr>
<tr>
<td>Password:</td>
<td><input type="password" name="Password" ID="Password"><br></td>
</tr>
<tr>
<td>DateOfBirth:</td>
<td><input type="text" name="dateOfBirth" ID="dateOfBirth"><br></td>
</tr>
<tr>
<td>Email:</td>
<td><input type="text" name="email" ID="email"><br></td>
</tr>
<tr>
<td>Address:</td>
<td><input type="text" name="address" ID="address"><br></td>
</tr>
<tr>
<td>Phone Number:</td>
<td><input type="text" name="phoneNumber" ID="phoneNumber"><br></td>
</tr>
<tr>
<td>Security Question:</td>
<td><input type="text" name="securityQuestion" ID="securityQuestion"><br></td>
</tr>
<tr>
<td>Answer:</td>
<td><input type="text" name="Answer" ID="Answer"><br></td>
</tr>
<tr>
<td>Role:</td>
<td><select name="Role" ID="Role">
<option value="User">Individual User</option>
<option value="Merchant">Merchant</option>
</select><br></td>
</tr>
<tr>
<td>Amount:</td>
<td><input type="text" name="SavingAccount" ID="SavingAccount"><br></td>
</tr>
</table>
 <br>
<br/>
<button type="submit">Submit</button>
<br/>
</form>
</body>
</html>