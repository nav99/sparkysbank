<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Transactions</title>
</head>
<body>
<br/>
<jsp:include page="DepartmentManager.jsp" />
<br/>
<br/>

<form:form method="POST"  modelAttribute="transactionModel" commandName="transactionModel" >
	<%-- action="${pageContext.servletContext.contextPath}/WelcomeCorporateOfficial/ApproveTransactions" --%>
<table border="1">
<tr>
<td bgcolor="#CCCCCC" align="center">Transaction ID</td>
<td bgcolor="#CCCCCC" align="center">Amount</td>
<td bgcolor="#CCCCCC" align="center">Receiver ID</td>
<td bgcolor="#CCCCCC" align="center">Sender ID</td>
<td bgcolor="#CCCCCC" align="center">Approve</td>
</tr>

<c:forEach var="transactions" items="${model2}">
<tr>
<td>${transactions.getTransactionId()}</td>
<td>${transactions.getAmount()}</td>
<td>${transactions.getReceiverId()}</td>
<td>${transactions.getSenderId()}</td>
<form:hidden path="transactionId" value="${transactions.getTransactionId()}" />
<td><input type="submit" value="Approve"></td>
</tr>
</c:forEach>
</table>
</form:form>
</body>
</html>