<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Account Info</title>
</head>
<body>
<br/>
<jsp:include page="DepartmentManager.jsp" />
<form name= transferUser method="POST" action="${pageContext.servletContext.contextPath}/TransferInternalUserSubmit">
<br/>
<table border="1">
<tr>
<td>Employee ID:</td>
<td><input type="text" name="User Name"><br></td>
</tr>
<tr>
<td>From Department:</td>
<td><input type="text" name="From Department"><br></td>
</tr>
<tr>
<td>To Department:</td>
<td><select name="DepartmentId" ID="DepartmentId">
<option value="1">Sales</option>
<option value="2">HR</option>
<option value="3">IT</option>
<option value="4">Transactions</option>
<option value="5">Management</option>
</select><br></td>
</tr>
</table>
<br/>
<button type="submit">Submit</button>
<br/>
</form>
</body>
</html>