<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Transactions</title>
</head>
<body>
<br/>
<jsp:include page="DepartmentManager.jsp" />
<form method ="post" action= "DepartmentManager/ViewPII">
<br/>
<br/>
<table border="1">
<tr>
<td>User Name</td>
<td>Name</td>
<td>Account Type</td>
<td>Role</td>
<td>Email</td>
</tr>

<c:forEach var="Activities" items="${msg}">
<tr>
<td>${Activities.getUsername()}</td>
<td>${Activities.getName()}</td>
<td>${Activities.getAccountType()}</td>
<td>${Activities.getRole()}</td>
<td>${Activities.getEmail()}</td>
</tr>
</c:forEach>
</table>
</form>
</body>
</html>