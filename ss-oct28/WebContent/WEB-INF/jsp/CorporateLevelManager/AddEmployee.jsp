<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
.error {
	color: #ff0000;
}
 
.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add User</title>
<script>
 function validateForm()
{
var x1=document.forms["addUser"]["UserID"].value;
 var x2=document.forms["addUser"]["Name"].value;
var x3=document.forms["addUser"]["Password"].value;
var x4=document.forms["addUser"]["email"].value;
var x5=document.forms["addUser"]["phoneNumber"].value;
var x6=document.forms["addUser"]["Salary"].value;
var atpos=x4.indexOf("@");
var dotpos=x4.lastIndexOf(".");
 

if (x1==null || x1=="" )
  {
  alert("Please enter the username/userid");
  return false;
  }
 /* if (x1.contains )
{
alert("Please enter the username/userid");
return false;
} */
if (x2==null || x2=="")
{
alert("Name cannot be blank");
return false;
}

if (x3==null || x3=="")
{
alert("Password cannot be blank");
return false;
}

if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x4.length)
{
alert("Not a valid e-mail address");
return false;
}

if (x5==null || x5=="")
{
alert("Phone number cannot be blank");
return false;
}

if (x5.length>10 || x5.length<10)
{
	alert("Phone number cannot exceed 10 digits");
}
if (x6==null || x6=="")
{
alert("Salary cannot be blank");
return false;

} 


}
</script>
</head>
<body>

<jsp:include page="CorporateManager.jsp" />
<br>
<form  method="POST" name="addUser" onsubmit="return validateForm()" action="${pageContext.servletContext.contextPath}/WelcomeCorporateOfficial/CorporateAddEmployee">
<br/>
<br>
<b>
<c:out value="${message}"/>
</b>
<br/>
<br/>
<table border="1">
<tr>
<td>User ID:</td>
<td><input type="text" name="UserID" ID="UserID"><br></td>
 <form:errors path="firstName"/>
</tr>
<tr>
<td>Name:</td>
<td><input type="text" name="Name" ID="Name"><br></td>
</tr>
<tr>
<td>Password:</td>
<td><input type="password" name="Password" ID="Password"><br></td>
</tr>
<tr>
<td>DateOfBirth:</td>
<td><input type="text" name="dateOfBirth" ID="dateOfBirth"><br></td>
</tr>
<tr>
<td>Email:</td>
<td><input type="text" name="email" ID="email"><br></td>
</tr>
<tr>
<td>Address:</td>
<td><input type="text" name="address" ID="address"><br></td>
</tr>
<tr>
<td>Phone Number:</td>
<td><input type="text" name="phoneNumber" ID="phoneNumber"><br></td>
</tr>

<!-- <tr>
<td>Account Type :</td>
<td><select name="accountType" ID="accountType">
<option value="Savings">Savings</option>
<option value="Checking">Checking</option>
</select><br></td>
</tr>
 -->
<tr>
<td>Security Question:</td>
<td><input type="text" name="securityQuestion" ID="securityQuestion"><br></td>
</tr>
<tr>
<td>Answer:</td>
<td><input type="text" name="Answer" ID="Answer"><br></td>
</tr>
<tr>
<td>Role:</td>
<td><select name="Role" ID="Role">
<option value="Employee">Employee</option>
<option value="Admin">Admin</option>
<option value="Manager">Manager</option>
<option value="Corporate">Corporate</option>
</select><br></td>
</tr>
<tr>
<td>DepartmentId:</td>
<td><select name="DepartmentId" ID="DepartmentId">
<option value="1">Sales</option>
<option value="2">HR</option>
<option value="3">IT</option>
<option value="4">Transactions</option>
<option value="5">Management</option>
</select><br></td>
</tr>
<tr>
<td>Salary:</td>
<td><input type="text" name="Salary" ID="Salary"><br></td>
</tr>
</table>

 <br>
<br/>
<button type="submit">Submit</button>
<br/>
</form>
</body>
</html>