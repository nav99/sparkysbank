<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Account Info</title>
<!-- <script>
function validateFormOnSubmit(theForm) {
	var reason = "";

	  reason += validateUsername(theForm.username);
	      
	  if (reason != "") {
	    alert("Some fields need correction:\n" + reason);
	    return false;
	  }

	  return true;
	}
function validateUsername(fld) {
    var error = "";
    var illegalChars = /\W/; // allow letters, numbers, and underscores
 
    if (fld.value == "") {
        fld.style.background = 'Yellow'; 
        error = "You didn't enter a username.\n";
    } else if ((fld.value.length < 5) || (fld.value.length > 15)) {
        fld.style.background = 'Yellow'; 
        error = "The username is the wrong length.\n";
    } else if (illegalChars.test(fld.value)) {
        fld.style.background = 'Yellow'; 
        error = "The username contains illegal characters.\n";
    } else {
        fld.style.background = 'White';
    } 
    return error;
}
</script> -->
</head>
<body>
<jsp:include page="CorporateManager.jsp" />
<br/>
<br>
<form name="registration" method ="post" onsubmit="return validateFormOnSubmit(this)" action="${pageContext.servletContext.contextPath}/WelcomeCorporateOfficial/CorporateModifyUser">
<br>
<b>
<c:out value="${message}"/>
</b>
<br/>
<br/>
<table border="1">
<tr><td>User ID:</td><td><input type="text" name="UserID"><br></td></tr>
<tr>
<td>DepartmentId:</td>
<td><select name="DepartmentId" ID="DepartmentId">
<option value="0">--</option>
<option value="1">Sales</option>
<option value="2">HR</option>
<option value="3">IT</option>
<option value="4">Transactions</option>
<option value="5">Management</option>
</select><br></td>
</tr>
<tr><td>Salary (Enter Null if no salary update):</td><td><input type="text" name="salary"><br></td></tr>
<tr><td><button type="submit">Modify</button></td> </tr>
</table>
</form>

</body>
</html>