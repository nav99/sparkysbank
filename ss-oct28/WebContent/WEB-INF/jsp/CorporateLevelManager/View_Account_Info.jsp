<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Account Info</title>
</head>
<body>
<br/>
<jsp:include page="CorporateManager.jsp" />
<br/><br/> 
<form method ="post" action= "WelcomeCorporateOfficial/CorporateViewAccountInfo">
<br>
<b>
<c:out value="${message}"/>
</b>
<br/>
<br/>
<table border="1">
<tr>
<td bgcolor="#CCCCCC" align="center">Account ID</td>
<td bgcolor="#CCCCCC" align="center">User Name</td>
<td bgcolor="#CCCCCC" align="center">Balance :</td>
<td bgcolor="#CCCCCC" align="center">User Type :</td>
</tr>

<c:forEach var="Activities" items="${msg}">
<tr>
<td>${Activities.getAccountId()}</td>
<td>${Activities.getUsername()}</td>
<td>${Activities.getBalance()}</td>
<td>${Activities.getUserType()}</td>
</tr>

</c:forEach>
</table>
</form>
</body>
</html>