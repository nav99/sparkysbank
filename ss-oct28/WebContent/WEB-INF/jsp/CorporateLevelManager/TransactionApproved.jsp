<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Transaction Updated</title>
</head>
<body>
<jsp:include page="CorporateManager.jsp"></jsp:include>
<p>Transaction Updated.</p>
<br/>
<a href="${pageContext.servletContext.contextPath}/WelcomeCorporateOfficial">Go to Home</a>

</body>
</html>