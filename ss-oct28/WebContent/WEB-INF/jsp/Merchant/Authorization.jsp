<%@ page session="false"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<%--   <link rel="stylesheet" href="<c:url value='/static/css/tutorial.css'/>" type="text/css" /> --%>
<title>Sparking Banking System</title>

</head>
<body>
	<jsp:include page="index.jsp" />
	<div>
		<ul id="mycontent">

			<li><br />
				<form name="form" method="post" action="/SSProject_October20/">
					<br> <b> <c:out value="${message}" />
					</b> <br /> <br /> <br />
					<b>Do you want your PII to be viewed by your officials?</b><br>
					<input type="submit"
						onClick="this.form.action='${pageContext.servletContext.contextPath}/MerchantEnableAuth'"
						value="Enable"> <br> <input type="submit"
						onClick="this.form.action='${pageContext.servletContext.contextPath}/MerchantDisableAuth'"
						value="Disable"> <br>
				</form></li>

		</ul>

	</div>
</body>
</html>
