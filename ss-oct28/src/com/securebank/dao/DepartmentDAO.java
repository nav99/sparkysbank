package com.securebank.dao;

import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.securebank.model.Account;
import com.securebank.model.Customers;
import com.securebank.model.Employee;
import com.securebank.model.Transaction;
import com.securebank.model.User;

/**
 * 
 * @author Sandeep
 * 
 */
@Transactional
@Repository
public class DepartmentDAO {
	
	private static final Logger logger = Logger
			.getLogger(CorporateDAO.class);

	
	private SessionFactory sessionFactory;
	@Autowired
	private HibernateTemplate hibernateTemplate;

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public String addUser(String UserID, String Name, String Password,
			String dateOfBirth, String email, String address, long phoneNumber,
			String securityQuestion, String Answer, String Role)
			throws HibernateException {
		String message = null;
		User user = new User();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			System.out.println("In DAO DEBUG" + email);
			java.util.Date date = new java.util.Date(System.currentTimeMillis()
					- (24 * 3600 * 1000));
			System.out.println("In DAO DEBUG2" + address);
			Timestamp timestamp = new Timestamp(date.getTime());
			System.out.println("In DAO DEBUG3" + address);
			user.setUsername(UserID);
			user.setAddress(address);
			user.setAccountStatus("open");
			user.setDateOfBirth(dateOfBirth);
			user.setEmail(email);
			user.setLastLoginTime(timestamp);
			user.setName(Name);
			user.setPassword(Password);
			user.setPhoneNumber(phoneNumber);
			user.setRole(Role);
			user.setSecurityQuestion(securityQuestion);
			user.setAnswer(Answer);

			System.out.println("In DAO DEBUG5" + Role);

			try {

				session.save(user);

				System.out.println("In DAO DEBUG7" + UserID);
				// session.getTransaction().commit();
				System.out.println("In DAO DEBUG8" + Name);
				// session.close();
				System.out.println("In DAO DEBUG9" + email);
				message = "User Added successfully";
				session.getTransaction().commit();
				session.close();

			} catch (Exception e) {
				message = "User is not added";
				System.out.println("Exception in commiting : " + e);
				e.printStackTrace();
				session.getTransaction().rollback();
				session.close();
			}

		} catch (Exception e) {
			message = "User is not added";
			System.out.println("In DAO exception");
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();

		}
		return message;
	}

	public String createEmployee(String username, Integer departmentId,
			double salary) throws HibernateException {
		String message = null;
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			Employee employee = new Employee();
			employee.setUsername(username);
			employee.setDepartmentId(departmentId);
			employee.setSalary(salary);
			employee.setAuthorization("NO");
			
			session.save(employee);
			message="Employee created successfully";
			session.getTransaction().commit();
			session.close();
		} catch (Exception e) {
			System.out.println("Exception in commiting : " + e);
			message = "Employee is not created";
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
		}
		return message;
	}

	public void deleteUser(String username) throws HibernateException {
		String message = null;
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try{
		User user = (User) session.load(User.class, username);
		if (null != user) {
			session.delete(user);
			message = "Employee deleted successfully";
			session.getTransaction().commit();
			session.close();
		}
		}catch (Exception e){
			message = "Employee is not deleted";
			session.getTransaction().rollback();
			session.close();
		}
	}

	public String updateInternalUser(String username, String departmentId,
			String salary) {
		String message = null;
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			String sql = "SELECT * FROM employee where username =  '"
					+ username + "'";
			SQLQuery query = session.createSQLQuery(
					sql);
			query.addEntity(Employee.class);
			Employee emp = (Employee) query.list().get(0);
			if (Integer.parseInt(departmentId) != 0) {
				emp.setDepartmentId(Integer.parseInt(departmentId));
			}
			if (!salary.equalsIgnoreCase("Null")) {
				emp.setSalary(Double.parseDouble(salary));
			}
			session.update(emp);
			message = "Internal User is updated successfully";

			session.close();
		} catch (Exception e) {
			e.printStackTrace();
			message = "User details are not updated";
			session.close();
		}
		return message;
	}

	@SuppressWarnings("unchecked")
	public List<Account> viewAccountInfo(String userID) {
		String message = null;
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			List<Account> acc = null;
			String sql = "select * from Account having username in(select username from Employee where departmentID =(select departmentId from employee where username = :username))";
			SQLQuery query = session.createSQLQuery(
					sql);
			query.setString("username", userID);
			System.out.println(userID);
			query.addEntity(Account.class);
			acc = (List<Account>) query.list();
			message = "Displays the Account information successfully";

			session.close();
			return acc;
		} catch (Exception e) {
			message = "Cannot diplay the account information";
			e.printStackTrace();
			session.close();
			return null;
		}

	}

	@SuppressWarnings("unchecked")
	public List<Transaction> viewTransactions(String userID) {
		String message = null;
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			List<Transaction> tran = null;
			String sql = "select * from transaction having (senderId in(select accountID from account where username in(select username from employee where departmentID =(select departmentId from employee where username = :username)))) or (receiverId in(select accountID from account where username in(select username from employee where departmentID =(select departmentId from employee where username = :username))))";
			SQLQuery query = session.createSQLQuery(
					sql);
			System.out.println("I'm checking for transactions query");
			query.setString("username", userID);
			System.out.println(userID);
			query.addEntity(Transaction.class);
			tran = (List<Transaction>) query.list();
			message = "Displays the list of transactions in the manager's department";
			session.close();
			return tran;
		} catch (Exception e) {
			message = "Cannot display the list of transactions";
			e.printStackTrace();
			session.close();
			return null;
		}

	}

	@SuppressWarnings("unchecked")
	public List<User> viewPII() {
		String message = null;
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			List<User> pii = null;
			String sql = "select * from user having username in (select username from customers where authorization = :param)";
			SQLQuery query = session.createSQLQuery(
					sql);
			query.setString("param", "Enabled");
			query.addEntity(User.class);
			pii = (List<User>) query.list();
			message = "Displays the list of Perosnal Information enabled to view";
			session.close();
			return pii;
		} catch (Exception e) {
			message = "Cannont view the PII";
			e.printStackTrace();

			session.close();
			return null;
		}

	}

	/* Authorization logic */
	@Autowired
	private Transaction transaction;
	@Autowired
	private Account account;

	public void setAccount(Account account) {
		this.account = account;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	
	public List<Transaction> authorize() {
		// TODO Auto-generated method stub
		List<Transaction> transactionsList = null;
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			String hql = "from Transaction as transaction where transaction.status=:statusVal";
			Query query = session.createQuery(hql);
			query.setString("statusVal", "manager_processing");
			transactionsList = query.list();
			
			Iterator<Transaction> transactionIterator = transactionsList.iterator();
			while (transactionIterator.hasNext()) {
				transaction = transactionIterator.next();
				logger.info("Transaction Id: "+ transaction.getTransactionId());
			}
		}	
		catch (HibernateException hibernateException) {
			logger.info("View Transaction by Department Manager Exception :" + hibernateException);
		}
		return transactionsList;
	}

	public Transaction approveTransaction(Integer transactionId) {
		// TODO Auto-generated method stub
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			transaction = (Transaction)session.load(Transaction.class, transactionId);
			double amount = transaction.getAmount();
			if (amount > 5000 && amount <= 8000) {
				transaction.setStatus("processed");
				transaction.setAmount(amount);
				List<Transaction> transactionList = null;
				String hqlAccId;
				hqlAccId = "from Transaction where transactionId = :transactionId";
				Query retreiveAccId = session.createQuery(hqlAccId);
				retreiveAccId.setLong("transactionId", transactionId);
				transactionList = retreiveAccId.list();
				debitSenderAccount(transactionList.get(0).getSenderId(), amount);
				creditReceiverAccount(transactionList.get(0).getReceiverId(), amount);
			}
			else 
				transaction.setStatus("corporate_processing");
			session.beginTransaction();
			session.update(transaction);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("deleteTransaction unsuccessful " + hibernateException);
			session.getTransaction().rollback();
			session.clear();
		}
		return transaction;
	}
	
	private void debitSenderAccount(long senderId, double updateAmount) {
		// TODO Auto-generated method stub
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			List<Account> accountList = null;

			//Retrieve Checking Account Balance of Receiver ID
			String hqlRetreiveReceiverCheckingAccountBalance = "from Account where accountId = :senderAccountId";
			Query queryRetreiveReceiverCheckingAccountBalance = session.createQuery(hqlRetreiveReceiverCheckingAccountBalance);
			queryRetreiveReceiverCheckingAccountBalance.setLong("senderAccountId", senderId);
			
			accountList = queryRetreiveReceiverCheckingAccountBalance.list();
			
			double senderCheckingBalance = accountList.get(0).getBalance();
			senderCheckingBalance -= updateAmount;
			account = (Account)session.load(Account.class, senderId);
			account.setBalance(senderCheckingBalance);
			
			//Updating Receiver Account checking balance.
			session.beginTransaction();
			session.update(account);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("Receiver Account update not done with exception"+ hibernateException);
			session.getTransaction().rollback();
			session.clear();
		}
	}

	private void creditReceiverAccount(long receiverId, double updateAmount) {
		// TODO Auto-generated method stub
		Session session = hibernateTemplate.getSessionFactory().openSession();
		List<Account> accountList = null;
		try {
			//Retrieve Account Balance of Receiver ID
			String hqlRetreiveReceiverAccountBalance;
			hqlRetreiveReceiverAccountBalance = "from Account where accountId = :receiverAccountId";
			Query queryRetreiveReceiverAccountBalance = session.createQuery(hqlRetreiveReceiverAccountBalance);
			queryRetreiveReceiverAccountBalance.setLong("receiverAccountId", receiverId);
			
			accountList = queryRetreiveReceiverAccountBalance.list();
			
			double receiverCheckingBalance = accountList.get(0).getBalance();
			receiverCheckingBalance += updateAmount;
			account = (Account)session.load(Account.class, receiverId);
			account.setBalance(receiverCheckingBalance);
			
			//Updating Receiver Account checking balance.
			session.beginTransaction();
			session.update(account);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("Sender Account update not done with exception"+ hibernateException);
			session.getTransaction().rollback();
			session.clear();
		}
		
	}
}

