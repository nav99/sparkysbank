package com.securebank.dao;

import java.util.Date;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.securebank.model.Account;
import com.securebank.model.Customers;
import com.securebank.model.Transaction;
import com.securebank.model.User;

public class UserDAO {

	private HibernateTemplate hibernateTemplate;

	private static Session session;

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public User findSampleByName(String username) throws HibernateException {
		try {
			List<User> results = hibernateTemplate.find("from User"
					+ " where username = ?", new Object[] { username });
			return results.size() > 0 ? (User) results.get(0) : null;
		} catch (Exception e) {
			return null;
		}
	}

	public String changePW(String username, String oldPW, String newPW,
			String confirmPW) {
		String msg = null;
		if (!newPW.equals(confirmPW)) {
			msg = "Confirm Password is not the same as new Password.";
		} else {
			String oldEncrtPW = DigestUtils.shaHex(oldPW);
			User user = new User();
			Session session = hibernateTemplate.getSessionFactory()
					.openSession();
			String hql = "from User as user where user.username=:username and user.password=:password";
			Query query = session.createQuery(hql);
			query.setString("username", username);
			query.setString("password", oldEncrtPW);
			List<User> list = query.list();
			java.util.Iterator<User> iter = list.iterator();
			while (iter.hasNext()) {
				user = iter.next();
			}
			if (user.getName() == null) {
				msg = "Old password is wrong. Please re-input.";
			} else {
				String newEncrtPW = DigestUtils.shaHex(newPW);
				user.setPassword(newEncrtPW);

				try {
					session.beginTransaction();
					session.saveOrUpdate(user);
					session.getTransaction().commit();
					msg = "Change Password successfully.";
					session.close();
				} catch (Exception e) {
					msg = "Change Password failed.";
					System.out.println("Exception in commiting : " + e);
					session.getTransaction().rollback();
					session.clear();
				}
			}
		}
		return msg;
	}
	
	public String changePW(String username,String newPW) {
		String msg = null;
			User user = new User();
			Session session = hibernateTemplate.getSessionFactory()
					.openSession();
			String hql = "from User as user where user.username=:username";
			Query query = session.createQuery(hql);
			query.setString("username", username);
			List<User> list = query.list();
			java.util.Iterator<User> iter = list.iterator();
			while (iter.hasNext()) {
				user = iter.next();
			}
			if (user.getName() == null) {
				msg = "Search user failed";
			} else {
				String newEncrtPW = DigestUtils.shaHex(newPW);
				user.setPassword(newEncrtPW);

				try {
					session.beginTransaction();
					session.saveOrUpdate(user);
					session.getTransaction().commit();
					msg = "Change Password successfully.";
					session.close();
				} catch (Exception e) {
					msg = "Change Password failed.";
					System.out.println("Exception in commiting : " + e);
					session.getTransaction().rollback();
					session.clear();
				}
			}
		return msg;
	}

	public String updateProfile(String username, String password,
			String newAddress, String email, String mobileNumber) {
		String msg = null;
		String encrtPassword = DigestUtils.shaHex(password);
		User user = new User();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		String hql = "from User as user where user.username=:username and user.password =:password";
		Query query = session.createQuery(hql);
		query.setString("username", username);
		query.setString("password", encrtPassword);
		List<User> list = query.list();
		java.util.Iterator<User> iter = list.iterator();
		while (iter.hasNext()) {
			user = iter.next();
		}
		if (user.getName() == null) {
			msg = "The password is not correct.";
		} else {
			user.setAddress(newAddress);
			user.setEmail(email);
			user.setPhoneNumber(Long.valueOf(mobileNumber));
			try {
				session.beginTransaction();
				session.saveOrUpdate(user);
				session.getTransaction().commit();
				msg = "Change profile successfully.";
				session.close();
			} catch (Exception e) {
				msg = "Change profile failed due to some reason.";
				System.out.println("Exception in commiting : " + e);
				session.getTransaction().rollback();
				session.clear();
			}
		}
		return msg;
	}

	public List<Transaction> getTransaction(String username) {
		Account account = new Account();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		String hql = "from Account as account where account.username=:username";
		Query query = session.createQuery(hql);
		query.setString("username", username);
		List<Account> list = query.list();
		java.util.Iterator<Account> iter = list.iterator();
		while (iter.hasNext()) {
			account = iter.next();
		}
		Long accountId = account.getAccountId();

		session = hibernateTemplate.getSessionFactory().openSession();
		String hql2 = "from Transaction as transaction where transaction.receiverId=:accountId or transaction.senderId=:accountId";
		Query query2 = session.createQuery(hql2);
		query2.setLong("accountId", accountId);
		List<Transaction> resultsList = query2.list();

		return resultsList;
	}

	public double getBalance(String username) {
		Account account = new Account();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		String hql = "from Account as account where account.username=:username";
		Query query = session.createQuery(hql);
		query.setString("username", username);
		List<Account> list = query.list();
		java.util.Iterator<Account> iter = list.iterator();
		while (iter.hasNext()) {
			account = iter.next();
		}
		return account.getBalance();
	}
	
	public long getAccountId(String username) {
		Account account = new Account();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		String hql = "from Account as account where account.username=:username";
		Query query = session.createQuery(hql);
		query.setString("username", username);
		List<Account> list = query.list();
		java.util.Iterator<Account> iter = list.iterator();
		while (iter.hasNext()) {
			account = iter.next();
		}
		return account.getAccountId();
	}

	public void Transfer(String username, String receiverID,
			String receiverName, double Amount) {
		Account senderAccount = new Account();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		String hql = "from Account as account where account.username=:username";
		Query query = session.createQuery(hql);
		query.setString("username", username);
		List<Account> list = query.list();
		java.util.Iterator<Account> iter = list.iterator();
		while (iter.hasNext()) {
			senderAccount = iter.next();
		}
		double senderBalance = senderAccount.getBalance();

		Account receiverAccount = new Account();
		String hql2 = "from Account as account where account.accountId=:accountId and account.username =:name";
		Query query2 = session.createQuery(hql2);
		query2.setString("accountId", receiverID);
		query2.setString("name", receiverName);
		List<Account> receiverList = query2.list();
		java.util.Iterator<Account> iter2 = receiverList.iterator();
		while (iter2.hasNext()) {
			receiverAccount = iter2.next();
		}

		if (receiverList.size() == 0) {
			System.out.println("Error");
		} else {
			Transaction transaction = new Transaction();
			transaction.setAmount(Amount);
			transaction.setReceiverId(receiverAccount.getAccountId());
			transaction.setSenderId(senderAccount.getAccountId());
			transaction.setTimeStamp(new Date());
			transaction.setType("debit");
			if (Amount <= 1000) {
				transaction.setStatus("processed");
			} else {
				transaction.setStatus("processing");
			}

			senderAccount.setBalance(senderBalance - Amount);
			double receiverBalance = receiverAccount.getBalance();
			receiverAccount.setBalance(receiverBalance + Amount);
			try {
				session.beginTransaction();
				session.saveOrUpdate(senderAccount);
				session.saveOrUpdate(receiverAccount);
				session.saveOrUpdate(transaction);
				session.getTransaction().commit();
				session.close();
			} catch (Exception e) {
				System.out.println("Exception in commiting : " + e);
				session.getTransaction().rollback();
				session.clear();
			}
		}
	}

	public String authorizeAccount(List<Transaction> list) {
		String msg = null;
		if (list.size() == 0) {
			msg = "No transaction information found.";
		} else {
			for (int i = 0; i < list.size(); i++) {
				Transaction transaction = new Transaction();
				session = hibernateTemplate.getSessionFactory().openSession();
				String hql = "from Transaction as transaction where transaction.transactionId=:id";
				Query query = session.createQuery(hql);
				query.setLong("id", list.get(i).getTransactionId());
				List<Transaction> transactionList = query.list();
				java.util.Iterator<Transaction> iter = transactionList
						.iterator();
				while (iter.hasNext()) {
					transaction = iter.next();
					transaction.setStatus("reviewing");
					try {
						session.beginTransaction();
						session.saveOrUpdate(transaction);
						session.getTransaction().commit();
						msg = "All transactions of this customer are authorized.";
						session.close();
					} catch (Exception e) {
						msg = "Authorize all the transactions failed.";
						System.out.println("Exception in commiting : " + e);
						session.getTransaction().rollback();
						session.clear();
					}
				}

			}
		}
		return msg;
	}

	public String payBills(String userName, long AccountNo, double Amount,
			String CompanyName) {
		String msg = null;
		Account senderAccount = new Account();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		String hql = "from Account as account where account.username=:username";
		Query query = session.createQuery(hql);
		query.setString("username", userName);
		List<Account> senderList = query.list();
		java.util.Iterator<Account> iter = senderList.iterator();
		while (iter.hasNext()) {
			senderAccount = iter.next();
		}
		double senderBalance = senderAccount.getBalance();

		if (senderBalance < Amount) {
			msg = "Your balance is not enough to pay for the bills.";
		} else {
			Account receiverAccount = new Account();
			String hql2 = "from Account as account where account.accountId=:accountId";
			Query query2 = session.createQuery(hql2);
			query2.setLong("accountId", AccountNo);
			List<Account> receiverlist = query2.list();
			java.util.Iterator<Account> iter2 = receiverlist.iterator();
			while (iter2.hasNext()) {
				receiverAccount = iter2.next();
			}

			Transaction transaction = new Transaction();

			if (Amount <= 1000) {
				transaction.setStatus("processed");
			} else {
				transaction.setStatus("processing");
			}
			if (receiverlist.size() == 0) {
				senderAccount.setBalance(senderBalance - Amount);
				transaction.setSenderId(senderAccount.getAccountId());
				transaction.setReceiverId(AccountNo);
				transaction.setAmount(Amount);
				transaction.setType("debit");
				transaction.setTimeStamp(new Date());
				try {
					session.beginTransaction();
					session.saveOrUpdate(senderAccount);
					session.saveOrUpdate(transaction);
					session.getTransaction().commit();
					msg = ("Payment successfully to " + CompanyName + ".");
					session.close();
				} catch (Exception e) {
					msg = ("Payment failed due to some reason.");
					System.out.println("Exception in commiting : " + e);
					session.getTransaction().rollback();
					session.clear();
				}
			} else {
				double receiverBalance = receiverAccount.getBalance();
				senderAccount.setBalance(senderBalance - Amount);
				receiverAccount.setBalance(receiverBalance + Amount);
				transaction.setSenderId(senderAccount.getAccountId());
				transaction.setReceiverId(receiverAccount.getAccountId());
				transaction.setAmount(Amount);
				transaction.setType("debit");
				transaction.setTimeStamp(new Date());
				try {
					session.beginTransaction();
					session.saveOrUpdate(senderAccount);
					session.saveOrUpdate(receiverAccount);
					session.saveOrUpdate(transaction);
					session.getTransaction().commit();
					msg = ("Payment successfully to " + CompanyName + ".");
					session.close();
				} catch (Exception e) {
					msg = ("Payment failed due to some reason.");
					System.out.println("Exception in commiting : " + e);
					session.getTransaction().rollback();
					session.clear();
				}
			}
		}
		return msg;
	}

	public String credit(String userName, double CreditAmount) {
		String msg = null;
		Account senderAccount = new Account();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		String hql = "from Account as account where account.username=:username";
		Query query = session.createQuery(hql);
		query.setString("username", userName);
		List<Account> senderList = query.list();
		java.util.Iterator<Account> iter = senderList.iterator();
		while (iter.hasNext()) {
			senderAccount = iter.next();
		}
		double senderBalance = senderAccount.getBalance();
		senderAccount.setBalance(senderBalance + CreditAmount);

		Transaction transaction = new Transaction();
		transaction.setAmount(CreditAmount);
		transaction.setSenderId(senderAccount.getAccountId());
		transaction.setReceiverId(senderAccount.getAccountId());
		transaction.setTimeStamp(new Date());
		transaction.setType("credit");
		if (CreditAmount <= 1000) {
			transaction.setStatus("processed");
		} else {
			transaction.setStatus("processing");
		}
		try {
			session.beginTransaction();
			session.saveOrUpdate(senderAccount);
			session.saveOrUpdate(transaction);
			session.getTransaction().commit();
			msg = "Credit successfully.";
			session.close();
		} catch (Exception e) {
			msg = "Credit Failed due to some reason.";
			System.out.println("Exception in commiting : " + e);
			session.getTransaction().rollback();
			session.clear();
		}
		return msg;
	}

	public Account getSenderAccount(String userName) {
		Account senderAccount = new Account();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		String hql = "from Account as account where account.username=:username";
		Query query = session.createQuery(hql);
		query.setString("username", userName);
		List<Account> list = query.list();
		java.util.Iterator<Account> iter = list.iterator();
		while (iter.hasNext()) {
			senderAccount = iter.next();
		}
		return senderAccount;
	}

	public String debit(String userName, double DebitAmount) {
		String msg = null;
		Account senderAccount = new Account();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		String hql = "from Account as account where account.username=:username";
		Query query = session.createQuery(hql);
		query.setString("username", userName);
		List<Account> senderList = query.list();
		java.util.Iterator<Account> iter = senderList.iterator();
		while (iter.hasNext()) {
			senderAccount = iter.next();
		}
		double senderBalance = senderAccount.getBalance();
		if (senderBalance < DebitAmount) {
			msg = "Your balance is not enough to do the debit.";
		} else {
			senderAccount.setBalance(senderBalance - DebitAmount);

			Transaction transaction = new Transaction();
			transaction.setAmount(DebitAmount);
			transaction.setReceiverId(senderAccount.getAccountId());
			transaction.setSenderId(senderAccount.getAccountId());
			transaction.setTimeStamp(new Date());
			transaction.setType("debit");
			if (DebitAmount <= 1000) {
				transaction.setStatus("processed");
			} else {
				transaction.setStatus("processing");
			}
			try {
				session.beginTransaction();
				session.saveOrUpdate(transaction);
				session.getTransaction().commit();
				msg = "Debit successfully.";
				session.close();
			} catch (Exception e) {
				msg = "Debit Failed due to some reason.";
				System.out.println("Exception in commiting : " + e);
				session.getTransaction().rollback();
				session.clear();
			}
		}
		return msg;
	}

	public String transfer(String userName, String receiverID, String Name,
			double Amount) {
		String msg;
		Account senderAccount = new Account();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		String hql = "from Account as account where account.username=:username";
		Query query = session.createQuery(hql);
		query.setString("username", userName);
		List<Account> list = query.list();
		java.util.Iterator<Account> iter = list.iterator();
		while (iter.hasNext()) {
			senderAccount = iter.next();
		}
		double senderBalance = senderAccount.getBalance();
		if (senderBalance < Amount) {
			msg = "Your balance is not enough to do the transfer.";
		} else {
			Account receiverAccount = new Account();
			String hql2 = "from Account as account where account.accountId=:accountId and account.username =:username";
			Query query2 = session.createQuery(hql2);
			query2.setString("accountId", receiverID);
			query2.setString("username", Name);
			List<Account> receiverlist = query2.list();
			java.util.Iterator<Account> iter2 = receiverlist.iterator();
			while (iter2.hasNext()) {
				receiverAccount = iter2.next();
			}
			if (receiverlist.size() == 0) {
				msg = "No receiver account found, please check your destination account number and name.";
			} else {
				double receiverBalance = receiverAccount.getBalance();

				senderAccount.setBalance(senderBalance - Amount);
				receiverAccount.setBalance(receiverBalance + Amount);
				Transaction transaction = new Transaction();
				if (Amount <= 1000) {
					transaction.setStatus("processed");
				} else {
					transaction.setStatus("processing");
				}
				transaction.setAmount(Amount);
				transaction.setSenderId(senderAccount.getAccountId());
				transaction.setReceiverId(receiverAccount.getAccountId());
				transaction.setType("debit");
				transaction.setTimeStamp(new Date());

				try {
					session.beginTransaction();
					session.saveOrUpdate(senderAccount);
					session.saveOrUpdate(receiverAccount);
					session.saveOrUpdate(transaction);
					session.getTransaction().commit();
					msg = ("Transfer successfully to Account "
							+ receiverAccount.getUsername() + ".");
					session.close();
				} catch (Exception e) {
					msg = ("Transfer failed due to some reason.");
					System.out.println("Exception in commiting : " + e);
					session.getTransaction().rollback();
					session.clear();
				}
			}
		}
		return msg;
	}

	public String enableAuth(String username) {
		String msg = null;
		Customers customers = new Customers();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		String hql = "from Customers as customers where customers.username=:username";
		Query query = session.createQuery(hql);
		query.setString("username", username);
		List<Customers> list = query.list();
		java.util.Iterator<Customers> iter = list.iterator();
		while (iter.hasNext()) {
			customers = iter.next();
		}
		customers.setAuthorization("enabled");

		try {
			session.beginTransaction();
			session.saveOrUpdate(customers);
			session.getTransaction().commit();
			msg = "PII Authorization enabled succesfully.";
			session.close();
		} catch (Exception e) {
			msg = "PII Authorization enabled failed due to some reason.";
			System.out.println("Exception in commiting : " + e);
			session.getTransaction().rollback();
			session.clear();
		}
		return msg;
	}

	public String disableAuth(String username) {
		String msg = null;
		Customers customers = new Customers();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		String hql = "from Customers as customers where customers.username=:username";
		Query query = session.createQuery(hql);
		query.setString("username", username);
		List<Customers> list = query.list();
		java.util.Iterator<Customers> iter = list.iterator();
		while (iter.hasNext()) {
			customers = iter.next();
		}
		customers.setAuthorization("disabled");

		try {
			session.beginTransaction();
			session.saveOrUpdate(customers);
			session.getTransaction().commit();
			msg = "PII Authorization disabled succesfully.";
			session.close();
		} catch (Exception e) {
			msg = "PII Authorization disabled failed due to some reason.";
			System.out.println("Exception in commiting : " + e);
			session.getTransaction().rollback();
			session.clear();
		}
		return msg;
	}

	// public String sendRequest(String userName, String AccountNo, double
	// Amount) {
	// String msg = null;
	// Account senderAccount = new Account();
	// Session session = hibernateTemplate.getSessionFactory().openSession();
	// String hql = "from Account as account where account.username=:username";
	// Query query = session.createQuery(hql);
	// query.setString("username", userName);
	// List<Account> senderList = query.list();
	// java.util.Iterator<Account> iter = senderList.iterator();
	// while (iter.hasNext()) {
	// senderAccount = iter.next();
	// }
	// double senderBalance = senderAccount.getBalance();
	//
	// if (senderBalance < Amount) {
	// msg = "Your balance is not enough to pay for the bills.";
	// } else {
	// Account receiverAccount = new Account();
	// String hql2 =
	// "from Account as account where account.accountId=:accountId";
	// Query query2 = session.createQuery(hql2);
	// query2.setLong("accountId", AccountNo);
	// List<Account> receiverlist = query2.list();
	// java.util.Iterator<Account> iter2 = receiverlist.iterator();
	// while (iter2.hasNext()) {
	// receiverAccount = iter2.next();
	// }
	//
	//
	// Transaction transaction = new Transaction();
	//
	// if (Amount <= 1000) {
	// transaction.setStatus("processed");
	// } else {
	// transaction.setStatus("processing");
	// }
	// if (receiverlist.size() == 0) {
	// senderAccount.setBalance(senderBalance - Amount);
	// transaction.setSenderId(senderAccount.getAccountId());
	// transaction.setAmount(Amount);
	// transaction.setType("debit");
	// transaction.setTimeStamp(new Date());
	// try {
	// session.beginTransaction();
	// session.saveOrUpdate(senderAccount);
	// session.saveOrUpdate(transaction);
	// session.getTransaction().commit();
	// msg = ("Payment successfully to " + CompanyName + ".");
	// session.close();
	// } catch (Exception e) {
	// msg = ("Payment failed due to some reason.");
	// System.out.println("Exception in commiting : " + e);
	// session.getTransaction().rollback();
	// session.clear();
	// }
	// } else {
	// double receiverBalance = receiverAccount.getBalance();
	// senderAccount.setBalance(senderBalance - Amount);
	// receiverAccount.setBalance(receiverBalance + Amount);
	// transaction.setSenderId(senderAccount.getAccountId());
	// transaction.setReceiverId(receiverAccount.getAccountId());
	// transaction.setAmount(Amount);
	// transaction.setType("debit");
	// transaction.setTimeStamp(new Date());
	// try {
	// session.beginTransaction();
	// session.saveOrUpdate(senderAccount);
	// session.saveOrUpdate(receiverAccount);
	// session.saveOrUpdate(transaction);
	// session.getTransaction().commit();
	// msg = ("Payment successfully to " + CompanyName + ".");
	// session.close();
	// } catch (Exception e) {
	// msg = ("Payment failed due to some reason.");
	// System.out.println("Exception in commiting : " + e);
	// session.getTransaction().rollback();
	// session.clear();
	// }
	// }
	// }
	// return msg;
	// }

}
