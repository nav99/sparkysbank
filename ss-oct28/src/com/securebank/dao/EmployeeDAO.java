package com.securebank.dao;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.securebank.model.Account;
import com.securebank.model.Employee;
import com.securebank.model.Transaction;
import com.securebank.model.User;
import com.securebank.web.EmployeeController;

/**
 * 
 * @author Sowmya

 *
 */
public class EmployeeDAO { 
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Autowired
	private SessionFactory sessionFactory;

	
	private static final int TRANSACTION_MONITORING_DEPARTMENT = 4;

	
	private HibernateTemplate hibernateTemplate;
	private Employee employee;
	private Transaction transaction;
	private Account account;
	private User user;

	
	@Autowired
	public void setUser(User user) {
		this.user = user;
	}

	
	@Autowired
	public void setAccount(Account account) {
		this.account = account;
	}

	@Autowired
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	
	@Autowired
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Autowired
	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	// get log4j handler
	private static final Logger logger = Logger
				.getLogger(EmployeeController.class);

	/**
	 * 
	 * @param userName
	 */
	public List<Transaction> verifyEmployeeDepartment(String userName) {

		
		logger.info("Employee userName :" + userName);
		// TODO Auto-generated method stub
		List<Transaction> transactionsList = null;
		try {
			Session session = hibernateTemplate.getSessionFactory().openSession();

			String hql = "from Employee as employee where employee.username=:userName";
			Query query = session.createQuery(hql);
			query.setString("userName", userName);
			List<Employee> list = query.list();
			Iterator<Employee> iter = list.iterator();
			while (iter.hasNext()) {
				employee = iter.next();
			}
			logger.info("Employee Department ID :" + employee.getDepartmentId());

			
			if (TRANSACTION_MONITORING_DEPARTMENT == employee.getDepartmentId()) {
				logger.info("Inside");
				hql = "from Transaction as transaction where transaction.status=:statusVal";
				query = session.createQuery(hql);
				query.setString("statusVal", "processing");
				transactionsList = query.list();

				
				Iterator<Transaction> transactionIterator = transactionsList.iterator();

				while (transactionIterator.hasNext()) {
					transaction = transactionIterator.next();
					logger.info("Transaction Id: "+ transaction.getTransactionId());

				}

			/*TO DO :: Need to implement else part of displaying access denied page or appropriate message

			that employee doesn't have rights to view it.*/


			
			}
				return transactionsList;


			}
			
		catch(HibernateException hibernateException){
			logger.info("Verify Employee Department exception :" + hibernateException);

			return null;
		}
		catch(Exception e){
			logger.info("Verify Employee Department exception :" + e);
			return null;
		}

	}

	public List<Transaction> retreiveTransactionDetails(Integer transactionId) {
		// TODO Auto-generated method stub
		List<Transaction> transaction = null;
		try {

		
		Session session = hibernateTemplate.getSessionFactory().openSession();

		String hql = "from Transaction as transaction where transaction.transactionId=:transactionId";
		Query query = session.createQuery(hql);
		query.setLong("transactionId", transactionId);

			transaction = query.list();

		
			session.close();
			return transaction;
		}
		catch(HibernateException exception){
			logger.info("retreiveTransactionDetails throws " +exception);
			return null;
		}

		
	}

	@SuppressWarnings("unchecked")
	public void updateModifiedTransaction(Integer transactionId,
			String updateAmount) {



		// TODO 3 Tasks. Update status, Debit sender amount, credit receiver amount.
		
		
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			transaction = (Transaction)session.load(Transaction.class, transactionId);


			
			// Modify status variable status.
			double amount = Double.parseDouble(updateAmount);
			if ( amount >= 1000 && amount <=5000) {
				transaction.setStatus("processed");
				transaction.setAmount(amount);
				List<Transaction> transactionList = null;
				String hqlAccId;
				hqlAccId = "from Transaction where transactionId = :transactionId";
				Query retreiveAccId = session.createQuery(hqlAccId);
				retreiveAccId.setLong("transactionId", transactionId);
				transactionList = retreiveAccId.list();
				debitSenderAccount(transactionList.get(0).getSenderId(), Double.parseDouble(updateAmount));

				creditReceiverAccount(transactionList.get(0).getReceiverId(), Double.parseDouble(updateAmount));

			}
			else {
				transaction.setStatus("manager_processing");
				transaction.setAmount(amount);
			}
			session.beginTransaction();
			session.update(transaction);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("retreiveTransactionDetails throws " +hibernateException);

			session.getTransaction().rollback();
			session.clear();
		}

		
	}

	private void creditReceiverAccount(long receiverId, double updateAmount) {
		// TODO Auto-generated method stub
		Session session = hibernateTemplate.getSessionFactory().openSession();
		List<Account> accountList = null;
		try {
			//Retrieve Account Balance of Receiver ID
			String hqlRetreiveReceiverAccountBalance;
			hqlRetreiveReceiverAccountBalance = "from Account where accountId = :receiverAccountId";


			Query queryRetreiveReceiverAccountBalance = session.createQuery(hqlRetreiveReceiverAccountBalance);
			queryRetreiveReceiverAccountBalance.setLong("receiverAccountId", receiverId);


			
			accountList = queryRetreiveReceiverAccountBalance.list();

			
			double receiverCheckingBalance = accountList.get(0).getBalance();
			receiverCheckingBalance += updateAmount;
			account = (Account)session.load(Account.class, receiverId);
			account.setBalance(receiverCheckingBalance);

			
			//Updating Receiver Account checking balance.
			session.beginTransaction();
			session.update(account);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("Sender Account update not done with exception"+ hibernateException);

			session.getTransaction().rollback();
			session.clear();
		}
	}

	private void debitSenderAccount(long senderId, double updateAmount) {
		// TODO Auto-generated method stub
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			List<Account> accountList = null;

			//Retrieve Checking Account Balance of Receiver ID
			String hqlRetreiveReceiverCheckingAccountBalance = "from Account where accountId = :senderAccountId";


			Query queryRetreiveReceiverCheckingAccountBalance = session.createQuery(hqlRetreiveReceiverCheckingAccountBalance);
			queryRetreiveReceiverCheckingAccountBalance.setLong("senderAccountId", senderId);


			
			accountList = queryRetreiveReceiverCheckingAccountBalance.list();

			
			double senderCheckingBalance = accountList.get(0).getBalance();
			senderCheckingBalance -= updateAmount;
			account = (Account)session.load(Account.class, senderId);
			account.setBalance(senderCheckingBalance);

			
			//Updating Receiver Account checking balance.
			session.beginTransaction();
			session.update(account);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("Receiver Account update not done with exception"+ hibernateException);

			session.getTransaction().rollback();
			session.clear();
		}
	}

	public void deleteTransaction(Integer transactionId) {
		logger.info("deleteTransaction entered " +transactionId);
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			transaction = (Transaction)session.load(Transaction.class, transactionId);

			// Modify status variable status.
			logger.info("deleteTransaction entered " +transaction.getReceiverId());

			double amount = transaction.getAmount();
			if (amount >= 1000 && amount <=5000) {
				transaction.setStatus("deleted");
			}
			else {
				transaction.setStatus("manager_delete_processing");
			}
			session.beginTransaction();
			session.update(transaction);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("deleteTransaction unsuccessful " + hibernateException);
			session.getTransaction().rollback();
			session.clear();
		}
	}

	public void approveTransaction(Integer transactionId) {
		// TODO Auto-generated method stub
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			transaction = (Transaction)session.load(Transaction.class, transactionId);

			double amount = transaction.getAmount();
			if (amount >= 1000 && amount <=5000) {
				transaction.setStatus("processed");
				transaction.setAmount(amount);
				List<Transaction> transactionList = null;
				String hqlAccId;
				hqlAccId = "from Transaction where transactionId = :transactionId";
				Query retreiveAccId = session.createQuery(hqlAccId);
				retreiveAccId.setLong("transactionId", transactionId);
				transactionList = retreiveAccId.list();
				debitSenderAccount(transactionList.get(0).getSenderId(), amount);
				creditReceiverAccount(transactionList.get(0).getReceiverId(), amount);

			}
			else {
				transaction.setStatus("manager_pending_processing");
			}
			session.beginTransaction();
			session.update(transaction);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("deleteTransaction unsuccessful " + hibernateException);
			session.getTransaction().rollback();
			session.clear();
		}
	}

	public void createTransfer(Transaction transaction2) throws Exception {
		// TODO Auto-generated method stub
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			double amount = transaction2.getAmount();
			if (amount >= 1000 && amount <=5000) {
				transaction2.setStatus("processed");
				//Retrieve Checking Account Balance of Receiver ID
				debitSenderAccount(transaction2.getSenderId(), amount);
				creditReceiverAccount(transaction2.getReceiverId(), amount);
			}
			else {
				transaction2.setStatus("manager_pending_processing");
			}
			session.beginTransaction();
			session.save(transaction2);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("deleteTransaction unsuccessful " + hibernateException);
			session.getTransaction().rollback();
			session.clear();

		} 
	}

	public void createCredit(Transaction transaction2) {
		// TODO Auto-generated method stub
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			double amount = transaction2.getAmount();
			if (amount >= 1000 && amount <=5000) {
				transaction2.setStatus("processed");
				//Retrieve Checking Account Balance of Receiver ID
				creditReceiverAccount(transaction2.getReceiverId(), amount);
			}
			else {
				transaction2.setStatus("manager_pending_processing");
			}
			session.beginTransaction();
			session.save(transaction2);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("deleteTransaction unsuccessful " + hibernateException);
			session.getTransaction().rollback();
			session.clear();


		} 
		
		
	}

	public void createDebit(Transaction transaction2) throws Exception {
		// TODO Auto-generated method stub
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			double amount = transaction2.getAmount();
			if (amount >= 1000 && amount <=5000) {
				transaction2.setStatus("processed");
				//Retrieve Checking Account Balance of Receiver ID
				debitSenderAccount(transaction2.getReceiverId(), amount);
			}
			else {
				transaction2.setStatus("manager_pending_processing");
			}
			session.beginTransaction();
			session.save(transaction2);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("deleteTransaction unsuccessful " + hibernateException);
			session.getTransaction().rollback();
			session.clear();

		} 
	}

	public void authorizeSystemAdmin(String userName) {
		// TODO Auto-generated method stub
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
		employee = (Employee)session.load(Employee.class, userName);
		employee.setAuthorization("enabled");

		session.beginTransaction();
		session.save(employee);
		session.getTransaction().commit();
		session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("Authorize System Admin unsuccessful " + hibernateException);

			session.getTransaction().rollback();
			session.clear();

		} 
	}

	
	public void unauthorizeSystemAdmin(String userName) {
		// TODO Auto-generated method stub
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			employee = (Employee)session.load(Employee.class, userName);
			employee.setAuthorization("disabled");
			session.beginTransaction();
			session.save(employee);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("Unauthorize System Admin unsuccessful " + hibernateException);

			session.getTransaction().rollback();
			session.clear();

		} 
	}

	public void updateUserProfile(User userObject, String userName) throws Exception{
		// TODO Auto-generated method stub
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			logger.info("User Name "+userName);
			user = (User)session.get(User.class, userName);
			user.setAddress(userObject.getAddress());
			user.setPhoneNumber(userObject.getPhoneNumber());
			session.beginTransaction();
			session.update(user);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("updateUserProfile unsuccessful " + hibernateException);
			session.getTransaction().rollback();
			session.clear();

		} 
	}

	
	public void addEmployee(Employee emp) {

		
		try {
			hibernateTemplate.getSessionFactory().openSession().save(emp);
		}
		catch(HibernateException exception)	{
			logger.info("Exception in Adding Employee" +exception);	
		}
	}

	
	@SuppressWarnings("unchecked")
	public List<Employee> RetrieveEmployeeDetails(){
		List<Employee> emp = null;
		String sql = "select * from employee having username in (select username from user where role!='Corporate')";
		SQLQuery query = hibernateTemplate.getSessionFactory().openSession().createSQLQuery(sql);

		query.addEntity(Employee.class);
		try {
			emp = query.list();


		}
		catch(HibernateException exception) {
			logger.info("retreiveTransactionDetails throws " +exception);
		}
		return emp;
		}

	
	@SuppressWarnings("unchecked")
	public List<Employee> displayResults() {
		List<Employee> emp = null;
		String sql = "select * from employee having username in(select username from user where (((role!='Manager') && (role!='Corporate') &&(role!='Admin'))))";
		Session session = hibernateTemplate.getSessionFactory().openSession();

		
		SQLQuery query = session.createSQLQuery(sql);
		query.addEntity(Employee.class);
		try {
			emp = query.list();
			session.close();
		}
		catch(HibernateException exception){
			logger.info("retreiveTransactionDetails throws " +exception);
			session.getTransaction().rollback();
			session.close();
		}
		return emp;
	}

	
	public void deleteEmployee(String username){
		Session session = hibernateTemplate.getSessionFactory().openSession();
		Employee emp = (Employee)session.load(Employee.class, username);
		try {
	        if (null != emp) {
	        	session.beginTransaction();
	        	session.delete(emp);
	        	session.getTransaction().commit();
	        	session.close();
	        }
		}
		catch(HibernateException hibernateException) {
			logger.info ("Delete employee by Department manager " + hibernateException);

			session.getTransaction().rollback();
			session.close();
		}
	}

public void deleteUser(String username) {
	Session session = hibernateTemplate.getSessionFactory().openSession();
	try {


		Employee employee = (Employee) session.load(Employee.class, username);
		if (null != employee) {
			session.beginTransaction();
			session.delete(employee);
			session.getTransaction().commit();
			session.close();



		}
	}

	catch ( HibernateException hibernateException) {
		logger.info ("Delete employee by Department manager " + hibernateException);
		session.getTransaction().rollback();
		session.close();




	}
	}

public void transferUser(String username, int departmentId){
	Session session = hibernateTemplate.getSessionFactory().openSession();
	try {
		String sql = "update employee set departmentId = ?, authorization = 'pending' where employee.username = ?";
		Employee employee = (Employee) session.createQuery(sql);
		session.getTransaction().commit();
		session.close();
	}
	catch( HibernateException hibernateException) {
		session.getTransaction().rollback();
		session.close();
	}
}

	public long getAccountId(String username) throws HibernateException {
		Session session = sessionFactory.openSession();
		try {

			String sql = "SELECT * FROM account where username =  '" + username
					+ "'";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Account.class);
			Account account = (Account) query.list().get(0);
			long accountid = account.getAccountId();
			session.close();
			return accountid;

		} catch (Exception e) {
			System.out.println("Exception in commiting : " + e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.clear();
			long a = 0;
			return a;

		}

	}
}