package com.securebank.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.securebank.model.Transaction;
import com.securebank.model.User;
import com.securebank.service.EmployeeService;

@Controller
public class EmployeeController {

	private EmployeeService employeeService;
	@Autowired
	private Transaction transaction;
	@Autowired
	private User user;

	public void setUser(User user) {
		this.user = user;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	@Autowired
	public void setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	// get log4j handler
	private static final Logger logger = Logger
			.getLogger(EmployeeController.class);

	@RequestMapping(value = "/Employee_Home", method = RequestMethod.GET)
	public ModelAndView sample(HttpServletRequest request) {
		HttpSession httpSession = request.getSession();
		String username = (String) httpSession.getAttribute("userName");
		long accountId = employeeService.getAccountId(username);
		ModelAndView model = new ModelAndView(
				"/Regular Employee/RegularEmployee", "accountId", accountId);
		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS");
		return model;
	}

	@RequestMapping(value = "/EmployeeTransactions", method = RequestMethod.GET)
	public ModelAndView employeeTransactions(
			@ModelAttribute("transactions") Transaction transaction,
			@ModelAttribute("transactionDummy") Transaction transactionDummy,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {

		HttpSession httpSession = request.getSession();
		String userName = (String) httpSession.getAttribute("userName");
		logger.info("UserName in Employee Controller :" + userName);
		// accountServiceImpl.changeAccount();
		model.addAttribute("transactions",
				employeeService.verifyEmployeeDepartment(userName));
		model.addAttribute("transactionDummy", new Transaction());
		ModelAndView modelAndView = new ModelAndView(
				"/Regular Employee/ProcessingTransactions", model);
		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS");
		return modelAndView;
	}

	@RequestMapping(value = "/EmployeeTransactions_ModifyTransaction", method = RequestMethod.POST)
	public ModelAndView employeeModifyTransactions(
			@ModelAttribute("transactionDummy") Transaction transactionDummy,
			ModelMap model,HttpServletRequest request) {
		if (logger.isInfoEnabled())
			logger.info("ENTERED EmployeeModifyTransactions CLASS");
		logger.info("Transaction ID : " + transactionDummy.getTransactionId());
		HttpSession httpSession = request.getSession();
		httpSession.setAttribute("transactionID",
				transactionDummy.getTransactionId());
		model.addAttribute("transaction",
				employeeService.retreiveTransactionDetails(transactionDummy
						.getTransactionId()));
		ModelAndView modelAndView = new ModelAndView(
				"/Regular Employee/ModifyTransactions", model);
		return modelAndView;
	}

	@RequestMapping(value = "/EmployeeTransactions_UpdateTransaction")
	public ModelAndView employeeUpdateTransactions(@RequestParam String updateAmount, HttpServletRequest request, ModelMap model) {
		if (logger.isInfoEnabled())
			logger.info("ENTERED Employee_UpdateTransaction CLASS ");
		HttpSession httpSession = request.getSession();
		Integer transactionId = (Integer) httpSession.getAttribute("transactionID");
		logger.info("Transaction Amount " + updateAmount +" ID" +transactionId);
		employeeService.updateModifiedTransaction(transactionId, updateAmount);
		/*model.addAttribute("transaction", employeeService.retreiveTransactionDetails(transactionId));*/
		ModelAndView modelAndView = new ModelAndView("/Regular Employee/TransactionUpdated",model);
		return modelAndView;
	}

	@RequestMapping(value = "/EmployeeTransactions_DeleteTransaction", method = RequestMethod.POST)
	public ModelAndView employeeDeleteTransactions(
			@ModelAttribute("transactionDummy") Transaction transactionDummy,
			ModelMap model,HttpServletRequest request) {
		String message=null;

		if (logger.isInfoEnabled())
			logger.info("ENTERED employeeDeleteTransactions GET CLASS");
		logger.info("Transaction ID : " + transactionDummy.getTransactionId());
		HttpSession httpSession = request.getSession();
		httpSession.setAttribute("transactionID",
				transactionDummy.getTransactionId());
		ModelAndView modelAndView = new ModelAndView(
				"/Regular Employee/DeleteTransactions");
		return modelAndView;
	}

	@RequestMapping(value = "/EmployeeTransactions_Deletetransaction", method = RequestMethod.GET)
	public ModelAndView employeeDeleteTransactions(HttpServletRequest request,
			ModelMap model) {
		if (logger.isInfoEnabled())
			logger.info("ENTERED employeeDeleteTransactions POST CLASS ");
		HttpSession httpSession = request.getSession();
		Integer transactionId = (Integer) httpSession
				.getAttribute("transactionID");
		employeeService.deleteTransaction(transactionId);
		/*
		 * model.addAttribute("transaction",
		 * employeeService.retreiveTransactionDetails(transactionId));
		 */
		ModelAndView modelAndView = new ModelAndView(
				"/Regular Employee/TransactionUpdated", model);
		return modelAndView;
	}

	@RequestMapping(value = "/EmployeeTransactions_ApproveTransaction", method = RequestMethod.POST)
	public ModelAndView employeeApproveTransactions(
			@ModelAttribute("transactionDummy") Transaction transactionDummy,
			HttpServletRequest request) {
	

		logger.info("ENTERED employeeApproveTransactions "
				+ transactionDummy.getTransactionId());
		employeeService.approveTransaction(transactionDummy.getTransactionId());
		/*
		 * model.addAttribute("transaction",
		 * employeeService.retreiveTransactionDetails(transactionId));
		 */
		ModelAndView modelAndView = new ModelAndView(
				"/Regular Employee/TransactionUpdated");
		return modelAndView;
	}
	@RequestMapping(value = "/EmployeeTransactions_InitiateCreateTransaction")
	public ModelAndView employeeInitiateCreateTransaction() {
		ModelAndView modelAndView = new ModelAndView(
				"/Regular Employee/CreateTransaction");
		return modelAndView;
	}

	@RequestMapping(value = "/EmployeeTransactions_InitiateCreateCredit", method = RequestMethod.GET)
	public ModelAndView employeeInitiateCreateCredit(
			@ModelAttribute("transactionModel") Transaction transactionModel,
			ModelMap model, HttpServletRequest request) {
		HttpSession httpSession = request.getSession();
		String username = (String) httpSession.getAttribute("userName");
		long accountId = employeeService.getAccountId(username);
		model.addAttribute("transactionModel", transaction);
		model.addAttribute("accountId", accountId);
		ModelAndView modelAndView = new ModelAndView(
				"/Regular Employee/CreateCredit", model);
		return modelAndView;
	}

	@RequestMapping(value = "/EmployeeTransactions_InitiateCreateDebit", method = RequestMethod.GET)
	public ModelAndView employeeInitiateCreateDebit(
			@ModelAttribute("transactionModel") Transaction transactionModel,
			ModelMap model, HttpServletRequest request) {
		HttpSession httpSession = request.getSession();
		String username = (String) httpSession.getAttribute("userName");
		long accountId = employeeService.getAccountId(username);
		model.addAttribute("transactionModel", transaction);
		model.addAttribute("accountId", accountId);
		ModelAndView modelAndView = new ModelAndView(
				"/Regular Employee/CreateDebit", model);
		return modelAndView;
	}

	@RequestMapping(value = "/EmployeeTransactions_InitiateCreateTransfer", method = RequestMethod.GET)
	public ModelAndView employeeInitiateCreateTransfer(
			@ModelAttribute("transactionModel") Transaction transactionModel,
			ModelMap model, HttpServletRequest request) {
		HttpSession httpSession = request.getSession();
		String username = (String)httpSession.getAttribute("userName");
		long accountId = employeeService.getAccountId(username);
		model.addAttribute("transactionModel", transaction);
		model.addAttribute("accountId", accountId);		ModelAndView modelAndView = new ModelAndView(
				"/Regular Employee/CreateTransfer", model);
		return modelAndView;
	}

	@RequestMapping(value = "/EmployeeTransactions_CreateCredit", method = RequestMethod.POST)
	public ModelAndView employeeCreateCredit(
			@ModelAttribute("transactionModel") Transaction transaction,BindingResult result) {
		String message = null;
		try {
			if(result.hasErrors()){
				message="Please enter values in proper format";
				return new ModelAndView("/Regular Employee/CreateCredit",
						"message", message);
	        }

			logger.info("Creating Credit transaction - by Employee");
/*			if (transaction.getAmount().toString().isEmpty()) {
				message = "Amount should not be blank";
				return new ModelAndView("/Regular Employee/CreateCredit",
						"message", message);

			} else if (transaction.getAmount().isNaN()) {
				message = "Amount should not be string";
				return new ModelAndView("/Regular Employee/CreateCredit",
						"message", message);

			}

			else if (transaction.getAmount() < 0) {
				message = "Amount should be greater than 0";
				return new ModelAndView("/Regular Employee/CreateCredit",
						"message", message);			} 
			
			else if (transaction.getReceiverId().toString().isEmpty()) {
				message = "Account Id should not be blank";
				return new ModelAndView("/Regular Employee/CreateCredit",
						"message", message);
				}
			else if (!validate_Number(transaction.getReceiverId().toString())) {
				message = "Amount should be number";
				return new ModelAndView("/Regular Employee/CreateCredit",
						"message", message);
			} else {
*/				employeeService.createCredit(transaction);
				message = "Transaction updated";
				return new ModelAndView("/Regular Employee/TransactionUpdated",
						"message", message);

			//}
		} catch (Exception e) {
			message = "Transaction cannot be updated";
			return new ModelAndView("/Regular Employee/TransactionUpdated",
					"message", message);
		}
	}

	@RequestMapping(value = "/EmployeeTransactions_CreateDebit", method = RequestMethod.POST)
	public ModelAndView employeeCreateDebit(
			@ModelAttribute("transactionModel") Transaction transaction,BindingResult result) {
		String message = null;
		try {
			if(result.hasErrors()){
				message="Please enter values in proper format";
				return new ModelAndView("/Regular Employee/CreateDebit",
						"message", message);
			}
		/*	if (transaction.getAmount().toString().isEmpty()) {
				message = "Amount should not be blank";
				return new ModelAndView("/Regular Employee/TransactionUpdated",
						"message", message);

			} else if (transaction.getAmount().isNaN()) {
				message = "Amount should not be string";
				return new ModelAndView("/Regular Employee/TransactionUpdated",
						"message", message);

			}

			else if (transaction.getAmount() < 0) {
				message = "Amount should be greater than 0";
				return new ModelAndView("/Regular Employee/TransactionUpdated",
						"message", message);
			} else if (transaction.getReceiverId().toString().isEmpty()) {
				message = "Account Id should not be blank";
				return new ModelAndView("/Regular Employee/TransactionUpdated",
						"message", message);
			} else if (!validate_Number(transaction.getReceiverId().toString())) {
				message = "Amount should be number";
				return new ModelAndView("/Regular Employee/TransactionUpdated",
						"message", message);

			} else {
*/				logger.info("Creating Debit Transaction - by Employee");
				employeeService.createDebit(transaction);
				return new ModelAndView("/Regular Employee/TransactionUpdated",
						"message", message);
//			}
		} catch (Exception e) {
			message = "Transaction cannot be updated";
			return new ModelAndView("/Regular Employee/TransactionUpdated",
					"message", message);
		}
	}

	@RequestMapping(value = "/EmployeeTransactions_CreateTransfer", method = RequestMethod.POST)
	public ModelAndView employeeCreateTransfer(
			@ModelAttribute("transactionModel") Transaction transaction,BindingResult result) {
		String message = null;
		try {
			if(result.hasErrors()){
				message="Please enter values in proper format";
				return new ModelAndView("/Regular Employee/CreateTransfer",
						"message", message);
			}
	/*		if (transaction.getAmount().toString().isEmpty()) {
				message = "Amount should not be blank";
				return new ModelAndView("/Regular Employee/TransactionUpdated",
						"message", message);

			} else if (transaction.getAmount().isNaN()) {
				message = "Amount should not be string";
				return new ModelAndView("/Regular Employee/TransactionUpdated",
						"message", message);

			}

			else if (transaction.getAmount() < 0) {
				message = "Amount should be greater than 0";
				return new ModelAndView("/Regular Employee/TransactionUpdated",
						"message", message);
			} else if (transaction.getReceiverId().toString().isEmpty()) {
				message = "Account Id should not be blank";
				return new ModelAndView("/Regular Employee/TransactionUpdated",
						"message", message);
			} else if (!validate_Number(transaction.getReceiverId().toString())) {
				message = "Amount should be number";
				return new ModelAndView("/Regular Employee/TransactionUpdated",
						"message", message);

			} else if (transaction.getSenderId().toString().isEmpty()) {
				message = "Account Id should not be blank";
				return new ModelAndView("/Regular Employee/TransactionUpdated",
						"message", message);
			} else if (!validate_Number(transaction.getSenderId().toString())) {
				message = "Amount should be number";
				return new ModelAndView("/Regular Employee/TransactionUpdated",
						"message", message);

			} else {*/

				logger.info("Creating Transfer transaction - by Employee");
				employeeService.createTransfer(transaction);
				message = "Transaction updated";
				return new ModelAndView("/Regular Employee/TransactionUpdated",
						"message", message);
			
		} catch (Exception e) {
			message = "Transaction could not be updated";
			return new ModelAndView("/Regular Employee/TransactionUpdated",
					"message", message);
		}
	}

	@RequestMapping(value = "/Employee_Authorize", method = RequestMethod.GET)
	public String employeeAuthorize() {
		logger.info("Authorize - by Employee");
		return "/Regular Employee/SystemAdminAuthorization";
	}

	@RequestMapping(value = "/Employee_AuthorizeSystemAdmin", method = RequestMethod.POST)
	public ModelAndView employeeAuthorizeSystemAdmin(HttpServletRequest request) {
		logger.info("Creating Transfer transaction - by Employee");
		HttpSession httpSession = request.getSession();
		String userName = (String) httpSession.getAttribute("userName");
		employeeService.authorizeSystemAdmin(userName);
		ModelAndView modelAndView = new ModelAndView(
				"/Regular Employee/SystemAdminAuthorized");
		return modelAndView;
	}

	@RequestMapping(value = "/Employee_UnauthorizeSystemAdmin", method = RequestMethod.POST)
	public ModelAndView employeeUnauthorizeSystemAdmin(
			HttpServletRequest request) {
		logger.info("Creating Transfer transaction - by Employee");
		HttpSession httpSession = request.getSession();
		String userName = (String) httpSession.getAttribute("userName");
		employeeService.unauthorizeSystemAdmin(userName);
		ModelAndView modelAndView = new ModelAndView(
				"/Regular Employee/SystemAdminUnauthorized");
		return modelAndView;
	}

	@RequestMapping(value = "/Employee_ViewUserAccounts", method = RequestMethod.GET)
	public ModelAndView employeeViewUserAccounts(
			@ModelAttribute("userDummy") User userDummy,
			HttpServletRequest request, ModelMap model) {
		logger.info("ViewUserAccounts - by Employee");
		model.addAttribute("userDummy", user);
		ModelAndView modelAndView = new ModelAndView(
				"/Regular Employee/EnterAccountNumber", model);
		return modelAndView;
	}

	@RequestMapping(value = "/Employee_ViewUserAccounts", method = RequestMethod.POST)
	public ModelAndView employeeViewUserAccount(
			@ModelAttribute("userDummy") User userDummy,BindingResult result, ModelMap model,
			HttpServletRequest request) {
		String message = null;
		try {
			if(result.hasErrors()){
				message="Please enter values in proper format";
				return new ModelAndView("/Regular Employee/EnterAccountNumber",
						"message", message);
			}
			if (userDummy.getUsername().isEmpty()) {
				message = "Please enter username";
				return new ModelAndView("/Regular Employee/EnterAccountNumber",
						"message", message);
			}

			logger.info("View User Account - by Employee "
					+ userDummy.getUsername());
			HttpSession session = request.getSession();
			session.setAttribute("username", userDummy.getUsername());
			model.addAttribute("userDummy", userDummy);
			return new ModelAndView("/Regular Employee/UserProfile", "message",
					message);
		} catch (Exception e) {
			message = "Invalid username";
			return new ModelAndView("/Regular Employee/EnterAccountNumber",
					"message", message);
		}
	}

	@RequestMapping(value = "/Employee_UpdateUserProfile", method = RequestMethod.POST)
	public ModelAndView employeeUpdateUserAccount(
			@ModelAttribute("userDummy") User user,BindingResult result, ModelMap model,
			HttpServletRequest request) {
		String message = null;
		try {
			if(result.hasErrors()){
				message="Please enter values in proper format";
				return new ModelAndView("/Regular Employee/CreateCredit",
						"message", message);
			}
			if( user.getAddress().isEmpty()) {
				message = "Enter a valid address";
				return new ModelAndView("/Regular Employee/UserProfile",
						"message", message);
			}
			else if (String.valueOf(user.getPhoneNumber()).isEmpty()) {
				message = "Enter a valid Phone Number";
				return new ModelAndView("/Regular Employee/UserProfile",
						"message", message);
			}
			else if (validate_phoneNumber(user.getPhoneNumber())) {
				message = "Enter a valid Phone Number of 10 digits";
				return new ModelAndView("/Regular Employee/UserProfile",
						"message", message);
			}
			else {
				message = "User Account updated Successfully";
				logger.info("Update User Account - by Employee "
						+ user.getAddress());
				employeeService.updateUserProfile(user, (String) request
						.getSession().getAttribute("username"));
				return new ModelAndView("/Regular Employee/UserProfileUpdated",
						"message", message);
			}
		} catch (Exception e) {
			message = "There is an error. Try again.";
			return new ModelAndView("/Regular Employee/UserProfileUpdated",
					"message", message);

		}
	}

	private boolean validate_Number(String phoneNumber) {
		// TODO Auto-generated method stub
		try {
			// long number =
			Double.parseDouble(phoneNumber);

			return false;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	private boolean validate_phoneNumber(long phoneNumber) {
		// TODO Auto-generated method stub
			try{
				//long number = 
				if(String.valueOf(phoneNumber).length()==10)
				{
				return true;	
				}
				return false;
			}catch(NumberFormatException e)
			{
				return false;
			}
		}
	
}
