package com.securebank.web;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.securebank.model.User;
import com.securebank.otp.SendMailSSL;
import com.securebank.otp.TOTP;
import com.securebank.service.OtpService;
import com.securebank.service.UserService;

@Controller
public class ForgotPWController {

	@Autowired
	private UserService userService;

	@Autowired
	private User user;

	public void setUser(User user) {
		this.user = user;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Autowired
	private OtpService otpService;

	public void setOtpService(OtpService otpService) {
		this.otpService = otpService;
	}

	@RequestMapping(value = "/ForgotPW_username_page", method = RequestMethod.POST)
	public ModelAndView ForgotPW_username() {
		return new ModelAndView("/otp/InputUsername");
	}

	@RequestMapping(value = "/ForgotPW_SQ_Page", method = RequestMethod.POST)
	public ModelAndView ForgotPW_SQ_Page(
			@ModelAttribute("userModel") User user, ModelMap model,
			@RequestParam String username) {
		User userObject = userService.searchByName(username);
		user__name = username;
		try {
			if (userObject == null) {
				return new ModelAndView("/otp/InputUsername", "message",
						"User not exists, please check your username.");
			} else {
				model.addAttribute("username", username);
				model.addAttribute("question", userObject.getSecurityQuestion());
				model.addAttribute("user", user);
				return new ModelAndView("/otp/SecurityQuestion");
			}
		} catch (Exception e) {
			model.addAttribute("username", username);
			model.addAttribute("question", userObject.getSecurityQuestion());
			model.addAttribute("user", user);
			return new ModelAndView("/otp/InputUsername");
		}
	}

	private String user__name;

	@RequestMapping(value = "/ForgotPW_securityQA", method = RequestMethod.POST)
	public ModelAndView ForgotPW_securityQA(HttpServletRequest request,
			HttpServletResponse response, ModelMap model,
			@RequestParam String username) {
		String question = null;
		try {
			User user = userService.searchByName(username);

			if (user == null) {
				return new ModelAndView("/otp/ResetPassword", "message",
						"Username not exists, please check.");
			} else {
				HttpSession httpSession = request.getSession();
				httpSession.setAttribute("userName", username);
				question = user.getSecurityQuestion();
				model.addAttribute("question", question);
				return new ModelAndView("/otp/SecurityQuestion");
			}
		} catch (Exception e) {
			model.addAttribute("question", question);
			model.addAttribute("username", username);
			return new ModelAndView("/otp/SecurityQuestion", "message",
					"Answer is not correct.");
		}
	}

	@RequestMapping(value = "/ForgotPW_QA_check_Page", method = RequestMethod.POST)
	public ModelAndView ForgotPW_QA_check(HttpServletRequest request,
			HttpServletResponse response, @RequestParam String username,
			String Answer, ModelMap model) {
		ModelAndView modelAndView;
		if (Answer.equals("") || null == Answer) {
			modelAndView = new ModelAndView("/otp/SecurityQuestion", "message",
					"Failed");
		} else {
			User user = userService.searchByName(username);
			String real_answer = user.getAnswer();
			String email_add = user.getEmail();
			if (real_answer.equals(Answer)) {
				model.addAttribute("username", username);
				String otp = TOTP.genOTP();
				Date date = new Date();
				try {
					boolean result = otpService.saveOtp(username, otp, date);
					boolean email_result = SendMailSSL
							.sendEmail(otp, email_add);
					if (email_result == true) {
						model.addAttribute("username", username);
						modelAndView = new ModelAndView("/otp/OTP", "message",
								"Email otp to " + email_add + " successfully.");
					} else {
						model.addAttribute("username", username);
						modelAndView = new ModelAndView(
								"/otp/SecurityQuestion", "message",
								"Email otp to " + email_add + " failed.");
					}
				} catch (Exception e) {
					model.addAttribute("username", username);
					modelAndView = new ModelAndView("/otp/SecurityQuestion",
							"message", "Failed");
				}

			} else {
				model.addAttribute("username", username);
				modelAndView = new ModelAndView("/otp/SecurityQuestion",
						"message", "Answer is not correct, please check.");
			}
		}
		return modelAndView;
	}

	@RequestMapping(value = "/ForgotPW_setPW", method = RequestMethod.POST)
	public ModelAndView ForgotPW_setPW(HttpServletRequest request,
			HttpServletResponse response, ModelMap model,
			@RequestParam String confirmOtp) {
		String username = user__name;
		ModelAndView modelAndView;
		String otp = otpService.getOtp(username);
		Date genDate = otpService.getGenDate(username);
		Date currentDate = new Date();
		long diffInMillies = (currentDate.getTime() - genDate.getTime()) / 1000;
		model.addAttribute("username", username);
		if (confirmOtp.equals("") || null == confirmOtp){ 
			model.addAttribute("username", username);
			modelAndView = new ModelAndView("/otp/OTP", "message",
					"otp is empty.");
		} else if (!otp.equals(confirmOtp)) {
			model.addAttribute("username", username);
			modelAndView = new ModelAndView("/otp/OTP", "message",
					"One time password is not correct, please check.");
		} else if (diffInMillies > 600) {
			model.addAttribute("username", username);
			modelAndView = new ModelAndView("/otp/OTP", "message",
					"One time password expired.");
		} else {
			model.addAttribute("username", username);
			modelAndView = new ModelAndView("/otp/SetPassword");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/ForgotPW_final", method = RequestMethod.POST)
	public ModelAndView ForgotPW_final(HttpServletRequest request,
			HttpServletResponse response, ModelMap model,
			@RequestParam String newPW, String confirmPW) {
		String username = user__name;
		ModelAndView modelAndView;
		if (newPW.equals("") || null == newPW) {
			model.addAttribute("username", username);
			modelAndView = new ModelAndView("/otp/SetPassword", "message",
					"Confirm otp is empty.");
		} else if (confirmPW.equals("") || null == confirmPW) {
			model.addAttribute("username", username);
			modelAndView = new ModelAndView("/otp/SetPassword", "message",
					"Confirm otp is empty.");
		} else if (!confirmPW.equals(newPW)) {
			model.addAttribute("username", username);
			modelAndView = new ModelAndView("/otp/SetPassword", "message",
					"Confirm password is different from password.");

		} else {
			String msg = userService.changePW(username, newPW);

			modelAndView = new ModelAndView("/login", "message",
					"Set Password successfully");
		}
		return modelAndView;
	}
}
