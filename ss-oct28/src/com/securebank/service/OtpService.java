package com.securebank.service;

import java.util.Date;

import com.securebank.dao.OtpDAO;

public class OtpService {

	private OtpDAO otpDAO;

	public void setOtpDAO(OtpDAO otpDAO) {
		this.otpDAO = otpDAO;
	}

	public boolean saveOtp(String username, String otp, Date date) {
		return otpDAO.saveOtp(username, otp, date);
	}

	public String getOtp(String username) {
		return otpDAO.getOtp(username);
	}

	public Date getGenDate(String username) {
		return otpDAO.getGenDate(username);
	}
}
