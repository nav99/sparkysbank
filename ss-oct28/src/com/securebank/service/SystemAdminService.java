package com.securebank.service;



//import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.securebank.dao.SystemAdminDAO;
import com.securebank.model.Systemadmin;
import com.securebank.model.User;
@Transactional
@Service
public class SystemAdminService {
	
	private SystemAdminDAO systemAdminDAO;

	@Autowired
	public void setSystemAdminDAO(SystemAdminDAO systemAdminDAO) {
		this.systemAdminDAO = systemAdminDAO;
	}



	public long addUser(String UserID, String Name, String Password,
			String dateOfBirth, String email, String address,
			String phoneNumber, String accountType, String securityQuestion,
			String Answer, String Role,String SavingAccount) throws NumberFormatException,Exception {
		String message =null;
			System.out.println("DEBUG in service" + Password);
			long phoneNo = Long.parseLong(phoneNumber);
			Double savingAccount= Double.parseDouble(SavingAccount);
			Password=DigestUtils.shaHex(Password);
			
			System.out.println("DEBUG in service2" + phoneNumber);
			message = systemAdminDAO.addUser(UserID, Name, Password,dateOfBirth, email, address, phoneNo, accountType,securityQuestion, Answer,Role);
			systemAdminDAO.createAccount(UserID, Role, savingAccount);
			if(message.contains("successful"))
			{
			if(Role.equalsIgnoreCase("User"))
			{
				message=systemAdminDAO.createCustomer(UserID);
			}
			else if(Role.equalsIgnoreCase("Merchant"))
			{
				message=systemAdminDAO.createMerchant(UserID);
			}
			}
			// Hibernate Modification :: calls Sample code to populate Account
			// Model class and create the table.
			System.out.println("DEBUG in service3" + dateOfBirth);
			long accountid=systemAdminDAO.getaccountId(UserID);
			return accountid;
		} 
		
				
	

	
	public long addInternalUser(String UserID, String Name, String Password,
			String dateOfBirth, String email, String address,
			String phoneNumber, String accountType, String securityQuestion,
			String Answer, String Role,String DepartmentId,String Salary) throws NumberFormatException,Exception{
		String message=null;
			System.out.println("DEBUG in service" + Password);
			long phoneNo = Long.parseLong(phoneNumber);
			Double salary= Double.parseDouble(Salary);
			Integer departmentId= Integer.parseInt(DepartmentId);
			Password=DigestUtils.shaHex(Password);
			
			System.out.println("DEBUG in service2" + phoneNumber);
			message = systemAdminDAO.addUser(UserID, Name, Password,dateOfBirth, email, address, phoneNo, accountType,securityQuestion, Answer,Role);
			double balance=1000;
			systemAdminDAO.createAccount(UserID,Role,balance);
			if(message.contains("successful"))
			{
			message= systemAdminDAO.createEmployee(UserID, departmentId, salary);
			}
			// Hibernate Modification :: calls Sample code to populate Account
			// Model class and create the table.
			System.out.println("DEBUG in service3" + dateOfBirth);
			long accountid=systemAdminDAO.getaccountId(UserID);
			return accountid;

	}
	
	public String deleteUser(String UserID) throws HibernateException {
		String message=null;
		try {
			System.out.println("DEBUG in service");
			String Role=systemAdminDAO.getRole(UserID);
			message = systemAdminDAO.deleteUser(UserID);
			if(message.contains("successful"))
			{
			if(Role.equalsIgnoreCase("User"))
			{
				message=systemAdminDAO.deleteCustomer(UserID);
			}
			else if(Role.equalsIgnoreCase("Merchant"))
			{	
				message=systemAdminDAO.deleteMerchant(UserID);
				
			}
			else if(Role.equalsIgnoreCase("Employee") || Role.equalsIgnoreCase("Corporate") || Role.equalsIgnoreCase("Manager") || Role.equalsIgnoreCase("Admin"))
			{	
				message=systemAdminDAO.deleteEmployee(UserID);
				
				
			}
			}
			System.out.println("DEBUG in service3");
			
		} catch (Exception e) {
			e.printStackTrace();
			message="User is not deleted";
		}
		return message;
	}


	public String modifyInternalUser(String username, String departmentId,
			String salary) {
		return systemAdminDAO.updateInternalUser(username, departmentId, salary );
	}


	public List<Systemadmin> accessLogfile() {
		// TODO Auto-generated method stub
		 

		return systemAdminDAO.accessLogfile(); 
		
	}


	public List<User> accessRequests() {
		// TODO Auto-generated method stub

		return systemAdminDAO.accessRequests();
	}


	public String updateUserRole(String userID, String role) {
		// TODO Auto-generated method stub
		
		return systemAdminDAO.updateUserRole(userID,role);
	}
}

